var bindive = require(__dirname + '/../bindive');
bindive.open
(
	__dirname + '/DATA1.BIN', function(error, file)
	{
		var i = 0;
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.cursors.default.direction = 'backward';
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.read_number('uint8', console.log.bind(null, i++));
		file.cursors.default.direction = 'forward';
		console.log(file.cursors);
	}
);
