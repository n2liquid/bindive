/*
	Copyright (C) 2013 Guilherme Vieira

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
var fs = require('fs');
function open(path, callback)
{
	fs.open
	(
		path, 'r', function(error, fd)
		{
			if(error)
			{
				callback(error);
				return;
			}
			callback(null, new File(fd));
		}
	);
}
function File(fd)
{
	this.fd = fd;
	this.cursors = { default: make_cursor(0, 'forward') };
}
function make_cursor(position, direction)
{
	return {
		position: position,
		direction: direction
	};
}
var number_types =
{
	'int8': { fn: 'readInt8', length: 1 },
	'uint8': { fn: 'readUInt8', length: 1 },
	'int16': { fn: 'readInt16LE', length: 2 },
	'uint16': { fn: 'readUInt16LE', length: 2 },
	'int32': { fn: 'readInt32LE', length: 4 },
	'uint32': { fn: 'readUInt32LE', length: 4 },
	'float': { fn: 'readFloatLE', length: 4 },
	'double': { fn: 'readDoubleLE', length: 8 }
};
var largest_number_length = number_types.double.length;
File.prototype.read_number = function File_read_number(cursor_name, type_name, callback)
{
	if(arguments.length === 2)
	{
		callback = arguments[1];
		type_name = arguments[0];
		cursor_name = 'default';
	}
	var cursor = this.cursors[cursor_name];
	var type = number_types[type_name];
	if(cursor === undefined)
	{
		callback(new Error("Unknown cursor '" + cursor_name + "'."));
		return;
	}
	if(type === undefined)
	{
		callback(new Error("Unknown number type '" + type_name + "'."));
		return;
	}
	var read_position_multiplier = { forward: 0, backward: -1 }[cursor.direction];
	var read_cursor_offset = type.length * read_position_multiplier;
	var read_position = cursor.position + read_cursor_offset;
	var new_position_multiplier = { forward: +1, backward: -1 }[cursor.direction];
	var new_cursor_offset = type.length * new_position_multiplier;
	cursor.position += new_cursor_offset;
	var buffer_offset = 0;
	var read_length = type.length;
	var read_buffer = new Buffer(largest_number_length);
	fs.read
	(
		this.fd,
		read_buffer,
		buffer_offset,
		read_length,
		read_position,
		function(error, bytes_read)
		{
			try
			{
				if(error)
				{
					throw error;
				}
				if(bytes_read < read_length)
				{
					throw new Error("Reached EOF while reading " + type_name + ".");
				}
				var number_read = read_buffer[type.fn](0);
				callback(null, number_read);
			}
			catch(error)
			{
				callback(error);
			}
		}
	);
}
module.exports =
{
	open: open,
	make_cursor: make_cursor
};
